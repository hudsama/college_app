class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name, null: false
      t.string :document, null: false
      t.integer :note, null: false
      t.references :course, foreign_key: true

      t.timestamps
    end

    add_index :students, :document, unique: true
    add_index :students, [:id, :course_id], unique: true
  end
end

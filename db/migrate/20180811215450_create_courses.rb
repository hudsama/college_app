class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'citext'

    create_table :courses do |t|
      t.citext :name, null: false
      t.integer :note
      t.references :college, foreign_key: true

      t.timestamps
    end

    add_index :courses, [:name, :college_id], unique: true
  end
end

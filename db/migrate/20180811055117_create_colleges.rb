class CreateColleges < ActiveRecord::Migration[5.2]
  def change
    enable_extension 'citext'

    create_table :colleges do |t|
      t.citext :name, null: false
      t.string :city, null: false
      t.integer :note

      t.timestamps
    end
    add_index :colleges, :name, unique: true
  end
end

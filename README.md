## College App
> This app was created for appprova test

### How to run project
- first clone this repo

####  create your databases
> `bundle exec rails db:create`

#### run migrations
`bundle exec rails db:migrate`

#### for access the dashboard should have a user. For this
> `rails db:seed`

> user email `admin@admin.com`

> user pass `12345678`

#### Run tests
`bundle exec rspec`

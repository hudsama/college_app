require 'rails_helper'

RSpec.describe "Update Student", type: :feature do
  describe 'update with valid data' do
    let!(:college) { create :college }
    let!(:course) { create :course, college: college }
    let!(:user) { create :user }

    it 'when all data is valid' do
      student = create(:student, course: course)

      login_as(user)

      visit students_path
      click_on t('actions.edit')

      fill_in t('labels.student.name'), with: 'Fake Student'
      click_on t('helpers.submit.student.update')

      assert_equal students_path, current_path
      assert page.has_text?(notice('student.update'))
      assert page.has_text?('Fake Student')
    end
  end

  describe 'should not be update when invalid data' do
    let!(:college) { create :college }
    let!(:course) { create :course, college: college }
    let!(:student) { create(:student, course: course) }
    let!(:user) { create :user }

    it 'when course name is blank' do
      login_as(user)

      visit students_path
      click_on t('actions.edit')

      fill_in t('labels.student.name'), with: ''
      click_on t('helpers.submit.student.update')

      assert_equal edit_student_path(student), current_path
      assert page.has_text?(t('form_error'))
    end

    it 'when note student is blank' do
      login_as(user)

      visit students_path
      click_on t('actions.edit')

      fill_in t('labels.student.note'), with: ''
      click_on t('helpers.submit.student.update')

      assert_equal edit_student_path(student), current_path
      assert page.has_text?(t('form_error'))
    end

    it 'when course is blank' do
      login_as(user)

      visit students_path
      click_on t('actions.edit')

      select('', from: 'student_course_id')
      click_on t('helpers.submit.student.update')

      assert_equal edit_student_path(student), current_path
      assert page.has_text?(t('form_error'))
    end
  end
end

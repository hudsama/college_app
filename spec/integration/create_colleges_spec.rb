require 'rails_helper'

RSpec.describe "Create new College", type: :feature do
  it 'when providing valid data' do
    user = (create :user)
    login_as(user)

    click_on t('actions.college.create')

    fill_in t('labels.college.name'), with: 'College BH'
    fill_in t('labels.college.city'), with: 'Belo Horizonte'
    fill_in t('labels.college.note'), with: 3
    click_on t('helpers.submit.college.create')

    assert_equal colleges_path, current_path
    assert page.has_text?(notice('college.create'))
  end

  it 'when providing invalid data' do
    user = (create :user)
    login_as(user)

    click_on t('actions.college.create')

    click_on t('helpers.submit.college.create')

    assert_equal college_path, current_path
    assert page.has_text?(t('form_error'))
  end
end

require 'rails_helper'

RSpec.describe "Update Course", type: :feature do
  describe 'update with valid data' do
    let!(:college) { create :college }
    let!(:user) { create :user }

    it 'when all data is valid' do
      login_as(user)

      visit courses_path
      course = create(:course, college: college)

      visit courses_path
      click_on t('actions.edit')

      fill_in t('labels.course.name'), with: 'Fake Course'
      click_on t('helpers.submit.course.update')

      assert_equal courses_path, current_path
      assert page.has_text?(notice('course.update'))
      assert page.has_text?('Fake Course')
    end
  end

  describe 'should not be update when invalid data' do
    let!(:college) { create :college }
    let!(:course) { create :course, college: college }
    let!(:user) { create :user }

    it 'when course name is blank' do
      login_as(user)
      visit courses_path

      click_on t('actions.edit')

      fill_in t('labels.course.name'), with: ''
      click_on t('helpers.submit.course.update')

      assert_equal edit_course_path(course), current_path
      assert page.has_text?(t('form_error'))
    end

    it 'when note course is blank' do
      login_as(user)
      visit courses_path

      click_on t('actions.edit')

      fill_in t('labels.course.note'), with: ''
      click_on t('helpers.submit.course.update')

      assert_equal edit_course_path(course), current_path
      assert page.has_text?(t('form_error'))
    end

    it 'when college is blank' do
      login_as(user)
      visit courses_path

      click_on t('actions.edit')

      select('', from: 'course_college_id')
      click_on t('helpers.submit.course.update')

      assert_equal edit_course_path(course), current_path
      assert page.has_text?(t('form_error'))
    end
  end
end

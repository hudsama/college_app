require 'rails_helper'

RSpec.describe "Update College", type: :feature do
  describe 'update with valid data' do
    let!(:user) { create :user }

    it 'when all data is valid' do
      college = create(:college)
      login_as(user)

      click_on t('actions.edit')
      fill_in t('labels.college.name'), with: 'College BH2'

      click_on t('helpers.submit.college.update')

      assert_equal colleges_path, current_path
      assert page.has_text?(notice('college.update'))
      assert page.has_text?('College BH2')
    end
  end

  describe 'update with invalid data' do
    let!(:user) { create :user }

    it 'when name data is blank' do
      college = create(:college)
      login_as(user)

      click_on t('actions.edit')
      fill_in t('labels.college.name'), with: ''

      click_on t('helpers.submit.college.update')

      assert_equal edit_college_path(college), current_path
      assert page.has_text?(t('form_error'))
    end

    it 'when city data is blank' do
      college = create(:college)
      login_as(user)

      visit colleges_path
      click_on t('actions.edit')
      fill_in t('labels.college.city'), with: ''

      click_on t('helpers.submit.college.update')

      assert_equal edit_college_path(college), current_path
      assert page.has_text?(t('form_error'))
    end

    it 'when note data is blank' do
      college = create(:college)
      login_as(user)

      visit colleges_path
      click_on t('actions.edit')
      fill_in t('labels.college.note'), with: ''

      click_on t('helpers.submit.college.update')

      assert_equal edit_college_path(college), current_path
      assert page.has_text?(t('form_error'))
    end
  end
end

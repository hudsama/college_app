require 'rails_helper'

RSpec.describe "Create new Course", type: :feature do
  it 'when providing valid data' do
    user = (create :user)
    create(:college)

    login_as(user)
    visit courses_path

    click_on t('actions.course.create')

    fill_in t('labels.course.name'), with: 'Engenharia de Software'
    fill_in t('labels.course.note'), with: 3
    select('Centro Universitario', from: 'course_college_id')

    click_on t('helpers.submit.course.create')

    assert_equal courses_path, current_path
    assert page.has_text?(notice('course.create'))
  end

end

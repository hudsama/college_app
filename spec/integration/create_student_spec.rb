require 'rails_helper'

RSpec.describe "Create new Student", type: :feature do
  describe 'create with valid params' do
    let!(:college) { create(:college) }
    let!(:course) { create(:course, college: college) }
    let!(:user) { create :user }

    it 'when providing valid data' do
      login_as(user)
      visit students_path

      click_on t('actions.student.create')

      fill_in t('labels.student.name'), with: 'Bruce W'
      fill_in t('labels.student.note'), with: 3
      fill_in t('labels.student.document'), with: 'dr12wex1504397'
      select('Engenharia de Software', from: 'student_course_id')
      click_on t('helpers.submit.student.create')

      assert_equal students_path, current_path
      assert page.has_text?(notice('student.create'))
    end
  end
end

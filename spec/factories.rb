FactoryBot.define do
  factory :college do
    name "Centro Universitario"
    city  "Belo Horizonte"
    note 5
  end

  factory :course do
    name "Engenharia de Software"
    note 5
    college
  end

  factory :student do
    name "John Doe"
    note 5
    document "xpto"
    course
  end

  factory :user do
    name 'John Doe'
    sequence(:email) {|n| "john#{n}@example.org" }
    password 'test'
    password_confirmation 'test'
  end
end

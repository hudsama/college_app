require 'rails_helper'

RSpec.describe Student, type: :model do
  subject { create :student }

  it { should validate_presence_of(:name) }
  it { should validate_inclusion_of(:note).in_range(0..5) }
  it { should belong_to(:course) }
  it { should validate_uniqueness_of(:id).scoped_to(:course_id) }
end

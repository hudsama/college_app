require 'rails_helper'

RSpec.describe College, type: :model do
  subject { create :college }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:city) }
  it { should validate_inclusion_of(:note).in_range(0..5) }
  it { should validate_uniqueness_of(:name).ignoring_case_sensitivity }
  it { should have_many(:courses) }

  describe 'vinculated courses on college' do
    let!(:college) { create :college, name: 'UNA', city: 'Divinopolis', note: 2 }
    let!(:course_1) { create :course, name: 'Software Engineering', college: college, note: 4 }
    let!(:course_2) { create :course, name: 'Administration', college: college, note: 3 }

    it 'should be contains courses' do
      expect(college.courses).to include(course_1)
      expect(college.courses).to include(course_2)
    end

    it 'should be deleted courses with college' do
      college.destroy
      expect(Course.all).to_not include(course_1)
      expect(Course.all).to_not include(course_2)
    end
  end
end

require 'rails_helper'

RSpec.describe Course, type: :model do
  subject { create :course }

  it { should validate_presence_of(:name) }
  it { should validate_inclusion_of(:note).in_range(0..5) }
  it { should belong_to(:college) }
  it { should validate_uniqueness_of(:name).scoped_to(:college_id) }
  it { should have_many(:students) }

  describe 'vinculated students on course' do
    let!(:college) { create :college, name: 'UNA', city: 'Divinopolis', note: 2 }
    let!(:course) { create :course, name: 'Software Engineering', college: college, note: 4 }
    let!(:student_1) { create :student, name: 'John Doe', document: 'xpto', note: 5, course: course }
    let!(:student_2) { create :student, name: 'Hudson C', document: 'xablu', note: 5, course: course }
    let!(:student_3) { create :student, name: 'Martha Wayne', document: 'batman', note: 5, course: course }

    it 'should be contains students' do
      expect(course.students).to include(student_1)
      expect(course.students).to include(student_2)
      expect(course.students).to include(student_3)
    end

    it 'should be deleted students with course' do
      course.destroy

      expect(Student.all).to_not include(student_1)
      expect(Student.all).to_not include(student_2)
      expect(Student.all).to_not include(student_3)
    end
  end
end

Rails.application.routes.draw do
  root 'results#index'
  get 'results', to: 'results#index'
  get 'search', to: 'results#search'

  get 'login', to: 'login#new'
  post 'login', to: 'login#create'
  delete 'logout', to: 'login#destroy'

  get 'colleges', to: 'colleges#index', as: 'colleges'
  get 'college', to: 'colleges#new'
  post 'college', to: 'colleges#create'
  get 'college/:id/edit', to: 'colleges#edit', as: 'edit_college'
  patch 'college/:id/edit', to: 'colleges#update', as: false
  delete 'college/:id/delete', to: 'colleges#destroy', as: 'delete_college'

  get 'courses', to: 'courses#index'
  get 'course', to: 'courses#new'
  post 'course', to: 'courses#create'
  get 'course/:id/edit', to: 'courses#edit', as: 'edit_course'
  patch 'course/:id/edit', to: 'courses#update', as: false
  delete 'course/:id/delete', to: 'courses#destroy', as: 'delete_course'

  get 'students', to: 'students#index'
  get 'student', to: 'students#new'
  post 'student', to: 'students#create'
  get 'student/:id/edit', to: 'students#edit', as: 'edit_student'
  patch 'student/:id/edit', to: 'students#update', as: false
  delete 'student/:id/delete', to: 'students#destroy', as: 'delete_student'
end

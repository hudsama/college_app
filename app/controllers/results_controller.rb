class ResultsController < ApplicationController
  has_scope :ordered_by_general_note, :default
  has_scope :order_by
  has_scope :find_by_name
  has_scope :find_by_college

  def index
    @results = apply_scopes(Course).all
  end

  def search
    if params[:course_name]
      redirect_to results_path(find_by_name: params[:course_name])
    else
      redirect_to results_path(find_by_college: params[:college_name])
    end
  end
end

class StudentsController < ApplicationController
  before_action :require_logged_user
  before_action :set_courses, only: [:new, :create, :edit, :update]
  before_action :set_student, only: [:edit, :update, :destroy]

  def index
    @students = Student.all
  end

  def new
    @student = Student.new
  end

  def edit
  end

  def update
    if @student.update(student_params)
      redirect_to students_path, notice: t('flash.student.update.notice')
    else
      render :edit
    end
  end

  def destroy
    @student.destroy!
    redirect_to students_path, notice: t('flash.student.destroy.notice')
  end

  def create
    @student = Student.new(student_params)
    if @student.save
      redirect_to students_path, notice: t('flash.student.create.notice')
    else
      render :new
    end
  end

  private

  def student_params
    params.require(:student).permit(:name, :note, :document, :course_id)
  end

  def set_courses
    @courses = Course.all
  end

  def set_student
    @student = Student.find(params[:id])
  end
end

class CoursesController < ApplicationController
  before_action :require_logged_user
  before_action :set_course, only: [:edit, :update, :destroy]
  before_action :set_colleges, only: [:new, :create, :edit, :update]

  def index
    @courses = Course.all
  end

  def new
    @course = Course.new
  end

  def edit
  end

  def update
    if @course.update(course_params)
      redirect_to courses_path, notice: t('flash.course.update.notice')
    else
      render :edit
    end
  end

  def destroy
    @course.destroy!
    redirect_to courses_path, notice: t('flash.course.destroy.notice')
  end

  def create
    @course = Course.new(course_params)
    if @course.save
      redirect_to courses_path, notice: t('flash.course.create.notice')
    else
      render :new
    end
  end

  private

  def course_params
    params.require(:course).permit(:name, :note, :college_id)
  end

  def set_course
    @course = Course.find(params[:id])
  end

  def set_colleges
    @colleges = College.all
  end
end

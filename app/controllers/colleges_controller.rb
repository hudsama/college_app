class CollegesController < ApplicationController
  before_action :require_logged_user
  before_action :set_college, only: [:edit, :update, :destroy]

  def index
    @colleges = College.all
  end

  def new
    @college = College.new
  end

  def edit
  end

  def update
    if @college.update(college_params)
      redirect_to colleges_path, notice: t('flash.college.update.notice')
    else
      render :edit
    end
  end

  def destroy
    @college.destroy!
    redirect_to colleges_path, notice: t('flash.college.destroy.notice')
  end

  def create
    @college = College.new(college_params)
    if @college.save
      redirect_to colleges_path, notice: t('flash.college.create.notice')
    else
      render :new
    end
  end

  private

  def college_params
    params.require(:college).permit(:name, :city, :note)
  end

  def set_college
    @college = College.find(params[:id])
  end
end

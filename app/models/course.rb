class Course < ApplicationRecord
  belongs_to :college
  has_many :students, dependent: :destroy

  validates_presence_of :name, :college
  validates_inclusion_of :note, in: 0..5
  validates :name, uniqueness: { scope: :college_id }

  scope :find_by_name, -> name { where("name ilike ?", name) }

  scope :find_by_college, -> name { joins(:college).where("colleges.name ilike ?", name) }

  scope :ordered_by_general_note, -> {
    joins(:college).order("colleges.note desc")
  }

  # scope :ordered_by_average_students, -> { where()}
  scope :order_by, -> field { order(note: :desc) }

  def average_student_grades
    self.students.present? ? self.students.sum(:note) / self.students.count : 0
  end
end

class Student < ApplicationRecord
  belongs_to :course

  validates_presence_of :name, :course, :document
  validates_uniqueness_of :document
  validates_inclusion_of :note, in: 0..5
  validates :id, uniqueness: {scope: :course_id }
end

class College < ApplicationRecord
  validates_presence_of :name, :city
  validates_inclusion_of :note, :in => 0..5
  validates_uniqueness_of :name

  has_many :courses, dependent: :destroy
end
